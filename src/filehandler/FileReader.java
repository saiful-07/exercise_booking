/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package filehandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import model.Booking;
import model.Exercise;
import model.ExerciseDate;
import model.ExerciseSession;
import model.User;
import util.Constants;

/**
 *
 * @author Iftekher Mahmud
 */
public class FileReader {

    public static List<String> getLinesFromFile(String fileName) {
        System.out.println("Entering getLinesFromFile with fileName --> " + fileName);
        List<String> lines = new ArrayList<String>();

        try {
            File myFile = new File(fileName);
            Scanner fileReader = new Scanner(myFile);
            while (fileReader.hasNextLine()) {
                String line = fileReader.nextLine();
                lines.add(line);
            }
            fileReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred. --> " + e.getMessage());
            e.printStackTrace();
        }
        
        System.out.println("Exiting getLinesFromFile with lines --> " + lines);
        return lines;
    }
    
    public static Map<String, Exercise> getAllExercises() {
        System.out.println("Entering getAllExercises");
        Map<String, Exercise> exercises = new HashMap<String, Exercise>();
        List<String> lines = getLinesFromFile(Constants.DATA_FILE_NAMES.EXERCISES_FILE);
        
        for (int i = 0; i < lines.size(); i++) {
            String[] line = lines.get(i).split("~");
            Exercise exercise = new Exercise(line[0], line[1], Double.parseDouble(line[2]));
            exercises.put(exercise.getExId(), exercise);
        }
        
        System.out.println("Exiting getAllExercises with exercises --> " + exercises);
        return exercises;
    }
    
    public static Map<String, ExerciseDate> getAllExDates() {
        System.out.println("Entering getAllExDates");
        Map<String, ExerciseDate> dates = new LinkedHashMap<String, ExerciseDate>();
        List<String> lines = getLinesFromFile(Constants.DATA_FILE_NAMES.DATES_FILE);
        
        for (int i = 0; i < lines.size(); i++) {
            String[] line = lines.get(i).split("~");
            ExerciseDate date = new ExerciseDate(line[0], line[1], line[2]);
            dates.put(date.getDateId(), date);
        }
        
        System.out.println("Exiting getAllExDates with dates --> " + dates);
        return dates;
    }
    
    public static Map<String, ExerciseSession> getAllExSessions() {
        System.out.println("Entering getAllExSessions");
        Map<String, ExerciseSession> sessions = new HashMap<String, ExerciseSession>();
        List<String> lines = getLinesFromFile(Constants.DATA_FILE_NAMES.SESSIONS_FILE);
        
        for (int i = 0; i < lines.size(); i++) {
            String[] line = lines.get(i).split("~");
            ExerciseSession session = new ExerciseSession(line[0], line[1], line[2]);
            sessions.put(session.getSessionId(), session);
        }
        
        System.out.println("Exiting getAllExSessions with sessions --> " + sessions);
        return sessions;
    }
    
    public static Map<String, Booking> getAllBookings() {
        System.out.println("Entering getAllBookings");
        Map<String, Booking> bookings = new HashMap<String, Booking>();
        Map<String, User> users = getAllUsers();
        Map<String, Exercise> exercises = getAllExercises();
        Map<String, ExerciseDate> dates = getAllExDates();
        Map<String, ExerciseSession> sessions = getAllExSessions();
        
        List<String> lines = getLinesFromFile(Constants.DATA_FILE_NAMES.BOOKINGS_FILE);
        
        for (int i = 0; i < lines.size(); i++) {
            String[] line = lines.get(i).split("~");
            
            Booking booking = new Booking();//(line[0], line[1], line[2], line[3], line[4]);
            booking.setBookingId(line[0]);
            booking.setUser(users.get(line[1]));//booking.getUser().setUsername(line[1]);
            booking.setExercise(exercises.get(line[2]));//booking.getExercise().setExId(line[2]);
            booking.setDate(dates.get(line[3]));//booking.getDate().setDateId(line[3]);
            booking.setSession(sessions.get(line[4]));//booking.getSession().setSessionId(line[4]);
            
            bookings.put(booking.getBookingId(), booking);
        }
        
        System.out.println("Exiting getAllBookings with bookings --> " + bookings);
        return bookings;
    }
    
    public static Map<String, Booking> getBookingsForUser(User user) {
        System.out.println("Entering getBookingsForUser with user --> " + user);
        Map<String, Booking> userBookings = new HashMap<String, Booking>();
        Map<String, Booking> allBookings = getAllBookings();
//        Map<String, Exercise> exercises = getAllExercises();
//        Map<String, ExerciseDate> dates = getAllExDates();
//        Map<String, ExerciseSession> sessions = getAllExSessions();
        
        for (Booking booking : allBookings.values()) {
            if(booking.getUser().getUsername().equals(user.getUsername())) {
//                booking.setUser(user);
//                booking.setExercise(exercises.get(booking.getExercise().getExId()));
//                booking.setDate(dates.get(booking.getDate().getDateId()));
//                booking.setSession(sessions.get(booking.getSession().getSessionId()));
                
                userBookings.put(booking.getBookingId(), booking);
            }
        }
        
        System.out.println("Exiting getBookingsForUser with userBookings --> " + userBookings);
        return userBookings;
    }
    
    public static Map<String, User> getAllUsers() {
        System.out.println("Entering getAllUsers");
        Map<String, User> users = new HashMap<String, User>();
        List<String> lines = getLinesFromFile(Constants.DATA_FILE_NAMES.USERS_FILE);
        
        for (int i = 0; i < lines.size(); i++) {
            String[] line = lines.get(i).split("~");
            User user = new User();
            user.setUsername(line[0]);
            user.setPassword(line[1]);
            
            users.put(user.getUsername(), user);
        }
        
        System.out.println("Exiting getAllBookings with bookings --> " + users);
        return users;
    }
    
    public static Map<String, User> getAllUsersWithBookings() {
        System.out.println("Entering getAllUsers");
        Map<String, User> users = getAllUsers();
        
        for (User user : users.values()) {            
            user.setBookings(getBookingsForUser(user));            
        }
        
        System.out.println("Exiting getAllBookings with bookings --> " + users);
        return users;
    }
}
