/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package filehandler;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import model.Booking;
import util.Constants;

/**
 *
 * @author Iftekher Mahmud
 */
public class FileEditor {

    public static boolean writeToFile(String filename, List<String> texts) {
        boolean isWritten = false;
        try {
            FileWriter editor = new FileWriter(filename);
            String textToWrite = "";
            for(int i = 0; i < texts.size(); i++) {
                textToWrite += texts.get(i);
                if (i < texts.size() - 1) {
                    textToWrite += "\n";
                }                
            }
            System.out.println("Writing Text --> " + textToWrite);
            editor.write(textToWrite);
            editor.close();
            
            isWritten = true;
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return isWritten;
    }
    
    public static boolean addBookingInFile(Booking newBooking) {
        boolean isWritten = false;
        
        Map<String, Booking> allBookings = FileReader.getAllBookings();
        allBookings.put(newBooking.getBookingId(), newBooking);
        
        //System.out.println("allBookings --> " + allBookings);
        List<String> allBookingTexts = new ArrayList<String>();
        
        for(Booking booking : allBookings.values()) {
            String bookingText = booking.getBookingId() + "~" + booking.getUser().getUsername() + "~" + booking.getExercise().getExId() + "~" + booking.getDate().getDateId() + "~" + booking.getSession().getSessionId();
            allBookingTexts.add(bookingText);
        }
        
        isWritten = writeToFile(Constants.DATA_FILE_NAMES.BOOKINGS_FILE, allBookingTexts);
        
        return isWritten;
    }
}
