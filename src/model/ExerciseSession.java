/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Iftekher Mahmud
 */
public class ExerciseSession {
    private String sessionId;
    private String dateId;
    private String session;

    public ExerciseSession() {
    }

    public ExerciseSession(String sessionId, String dateId, String session) {
        this.sessionId = sessionId;
        this.dateId = dateId;
        this.session = session;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getDateId() {
        return dateId;
    }

    public void setDateId(String dateId) {
        this.dateId = dateId;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    @Override
    public String toString() {
        return session;
    }
}
