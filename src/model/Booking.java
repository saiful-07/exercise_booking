/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Iftekher Mahmud
 */
public class Booking {
    private String bookingId;
    private User user;
    private Exercise exercise;
    private ExerciseDate date;
    private ExerciseSession session;

    public Booking() {
    }

    public Booking(String bookingId, User user, Exercise exercise, ExerciseDate date, ExerciseSession session) {
        this.bookingId = bookingId;
        this.user = user;
        this.exercise = exercise;
        this.date = date;
        this.session = session;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public User getUser() {
        if(user == null) {
            user = new User();
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Exercise getExercise() {
        if(exercise == null) {
            exercise = new Exercise();
        }
        return exercise;
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }

    public ExerciseDate getDate() {
        if(date == null) {
            date = new ExerciseDate();
        }
        return date;
    }

    public void setDate(ExerciseDate date) {
        this.date = date;
    }

    public ExerciseSession getSession() {
        if(session == null) {
            session = new ExerciseSession();
        }
        return session;
    }

    public void setSession(ExerciseSession session) {
        this.session = session;
    }

    @Override
    public String toString() {
        return "Booking{" + "bookingId=" + bookingId + ", user=" + user + ", exercise=" + exercise + ", date=" + date + ", session=" + session + '}';
    }
}
