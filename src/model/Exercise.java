/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Iftekher Mahmud
 */
public class Exercise {
    private String exId;
    private String exName;
    private double cost;

    public Exercise() {
    }

    public Exercise(String exId, String exName, double cost) {
        this.exId = exId;
        this.exName = exName;
        this.cost = cost;
    }

    public String getExId() {
        return exId;
    }

    public void setExId(String exId) {
        this.exId = exId;
    }

    public String getExName() {
        return exName;
    }

    public void setExName(String exName) {
        this.exName = exName;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return exName;
    }
}
