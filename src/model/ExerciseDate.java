/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Iftekher Mahmud
 */
public class ExerciseDate {
    private String dateId;
    private String date;
    private String day;

    public ExerciseDate() {
    }

    public ExerciseDate(String dateId, String date, String day) {
        this.dateId = dateId;
        this.date = date;
        this.day = day;
    }

    public String getDateId() {
        return dateId;
    }

    public void setDateId(String dateId) {
        this.dateId = dateId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return date + ", " + day;
    }
}
