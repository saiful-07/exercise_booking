/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Iftekher Mahmud
 */
public class User {
    private String username;
    private String password;
    Map<String, Booking> bookings;

    public User() {
    }

    public User(String username, String password, Map<String, Booking> bookings) {
        this.username = username;
        this.password = password;
        this.bookings = bookings;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<String, Booking> getBookings() {
        if(bookings == null) {
            bookings = new HashMap<String, Booking>();
        }
        return bookings;
    }

    public void setBookings(Map<String, Booking> bookings) {
        this.bookings = bookings;
    }

    @Override
    public String toString() {
        return "User{" + "username=" + username + ", password=" + password + "}";
    }
}
