/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util;

/**
 *
 * @author Iftekher Mahmud
 */
public class Constants {
    public class UI_NAMES {
        public static final String LOGIN_PANEL = "LoginFXML.fxml";
        public static final String BOOKING_PANEL = "BookExerciseFXML.fxml";
        public static final String REPORT_PANEL = "ReportsFXML.fxml";
        public static final String CSS = "/css/book_exercise.css";
    }
    
    public class DATA_FILE_NAMES {
        public static final String USERS_FILE = "users.txt";
        public static final String EXERCISES_FILE = "exercises.txt";
        public static final String DATES_FILE = "dates.txt";
        public static final String SESSIONS_FILE = "sessions.txt";
        public static final String BOOKINGS_FILE = "bookings.txt";
    }
    
    public class ERR_MSG {
        public static final String LOGIN_FAILED = "Login Failed! Invalid Username or Password!";
        public static final String EMPTY_USER = "Something Went Wrong! User can't be Empty";
        public static final String EMPTY_EXERCISE = "You Must Select An Exercise";
        public static final String EMPTY_DATE = "You Must Select An Exercise Date";
        public static final String EMPTY_SESSION = "You Must Select An Exercise Session";
        public static final String CONFLICT_SESSION = "Sorry! You Will Have Time Conflict With This Session";
        public static final String BOOKING_SUCCESS = "Congratulation! You Have Booked Successfully";
        public static final String BOOKING_FAILURE = "Sorry! Unable to Book";
    }
    
    public class FILTER_TYPES {
        public static final int FILTER_ALL = 1;
        public static final int FILTER_EXERCISE = 2;
        public static final int FILTER_EXERCISE_DATE = 3;
        public static final int FILTER_EXERCISE_SESSION = 4;
    }
}
