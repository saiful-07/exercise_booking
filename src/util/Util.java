/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util;

import excercisebooking.BookExerciseFXMLController;
import excercisebooking.ExerciseBooking;
import excercisebooking.ReportsFXMLController;
import filehandler.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import model.Booking;
import model.User;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Iftekher Mahmud
 */
public class Util {

    public static FXMLLoader showPanel(Class className, String panelName) throws IOException {
        System.out.println("Trying to load --> " + panelName + " -- from --> " + className.getName());
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(className.getResource(panelName));
        loader.load();
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        //scene.getStylesheets().add(getClass().getResource(Constants.UI_NAMES.CSS).toExternalForm());

        ExerciseBooking.getMainStage().setScene(scene);
        ExerciseBooking.getMainStage().show();
        
        System.out.println("New panel shown --> " + panelName);
        return loader;
    }

    public static String getHashString(String real) {
        System.out.println("Entering getHashString with real");
        StringBuffer hexString = new StringBuffer();
        MessageDigest digest;
        try {
            digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(real.getBytes());
            byte messageDigest[] = digest.digest();

            for (int i = 0; i < messageDigest.length; i++) {
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            }            
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Exiting getHashString with hexString --> " + hexString);
        return hexString.toString();
    }
    
    public static void showBookingPanel(Class className, String panelName, User user) throws IOException {
        FXMLLoader loader = showPanel(className, panelName);
        BookExerciseFXMLController bookController = loader.getController();
        bookController.setUser(user);
    }
    
    public static void showReportPanel(Class className, String panelName, User user) throws IOException {
        FXMLLoader loader = showPanel(className, panelName);
        ReportsFXMLController reportController = loader.getController();
        reportController.setUser(user);
    }
    
    public static String getNewBookingId() {
        String bookId = null;
        
        Map<String, Booking> allBookings = FileReader.getAllBookings();
        int num = allBookings.size()+1;
        bookId = "BK" + StringUtils.leftPad(num+"", 4, "0");
        
        return bookId;
    }
}
