/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package excercisebooking;

import filehandler.FileEditor;
import filehandler.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import model.Booking;
import model.Exercise;
import model.ExerciseDate;
import model.ExerciseSession;
import model.User;
import util.Constants;
import util.Util;

/**
 * FXML Controller class
 *
 * @author Iftekher Mahmud
 */
public class BookExerciseFXMLController implements Initializable {

    @FXML
    private Text successMsgText;
    @FXML
    private Text failureMsgText;
    @FXML
    private ComboBox<Exercise> exNameDrop;
    @FXML
    private ComboBox<ExerciseDate> dateDrop;
    @FXML
    private ComboBox<ExerciseSession> sessionDrop;
    @FXML
    private TextField costField;
    @FXML
    private Button cancelButton;
    @FXML
    private Button bookButton;
    @FXML
    private TableView<Booking> myBookingTable;
    @FXML
    private TableColumn<Booking, String> exIdCol;
    @FXML
    private TableColumn<Booking, String> exNameCol;
    @FXML
    private TableColumn<Booking, String> dateCol;
    @FXML
    private TableColumn<Booking, String> sessionCol;
    @FXML
    private TableColumn<Booking, Number> costCol;
    @FXML
    private Button reportsButton;
    @FXML
    private Button logoutButton;

    private User user;

    private Exercise exercise;
    private ExerciseDate date;
    private ExerciseSession session;

    private ObservableList<Booking> allBookings;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("Entering initialize Booking Panel");

        costField.setText("0.0");
        costField.setEditable(false);

        setAllDropdownValues();

        allBookings = FXCollections.observableArrayList();

        System.out.println("Exiting initialize Booking Panel");
    }

    @FXML
    private void handleExNameAction(ActionEvent event) {
        System.out.println("Entering handleExNameAction Booking Panel");

        exercise = exNameDrop.getSelectionModel().getSelectedItem();
        System.out.println("selExercise --> " + exercise);

        if (exercise != null) {
            costField.setText(exercise.getCost() + "");
        }

        resetExerciseDateDropdown();

        System.out.println("Exiting handleExNameAction Booking Panel");
    }

    @FXML
    private void handleDateAction(ActionEvent event) {
        System.out.println("Entering handleDateAction Booking Panel");

        date = dateDrop.getSelectionModel().getSelectedItem();
        System.out.println("selDate --> " + date);

        resetExerciseSessionDropdown();

        System.out.println("Exiting handleDateAction Booking Panel");
    }

    @FXML
    private void handleSessionAction(ActionEvent event) {
        System.out.println("Entering handleSessionAction Booking Panel");

        session = sessionDrop.getSelectionModel().getSelectedItem();

        System.out.println("Exiting handleSessionAction Booking Panel");
    }

    @FXML
    private void handleCancelAction(ActionEvent event) {
        System.out.println("Entering handleCancelAction Booking Panel");

        resetAll();

        System.out.println("Exiting handleCancelAction Booking Panel");
    }

    @FXML
    private void handleBookAction(ActionEvent event) {
        System.out.println("Entering handleBookAction Booking Panel");

        resetMsg();

        if (user == null) {
            showErrorMsg(Constants.ERR_MSG.EMPTY_USER);
        } else if (exercise == null) {
            showErrorMsg(Constants.ERR_MSG.EMPTY_EXERCISE);
        } else if (date == null) {
            showErrorMsg(Constants.ERR_MSG.EMPTY_DATE);
        } else if (session == null) {
            showErrorMsg(Constants.ERR_MSG.EMPTY_SESSION);
        } else if (sessionTimeConflict(session)) {
            showErrorMsg(Constants.ERR_MSG.CONFLICT_SESSION);
        } else {
            Booking booking = new Booking();
            booking.setBookingId(Util.getNewBookingId());
            booking.setUser(user);
            booking.setExercise(exercise);
            booking.setDate(date);
            booking.setSession(session);

            user.getBookings().put(booking.getBookingId(), booking);

            if (FileEditor.addBookingInFile(booking)) {
                showSuccessMsg(Constants.ERR_MSG.BOOKING_SUCCESS);
                resetFields();
            } else {
                showSuccessMsg(Constants.ERR_MSG.BOOKING_FAILURE);
            }

            setBookingTableView();
        }

        System.out.println("Exiting handleBookAction Booking Panel");
    }

    @FXML
    private void handleReportsAction(ActionEvent event) throws IOException {
        System.out.println("Entering handleReportsAction Booking Panel");

        Util.showReportPanel(getClass(), Constants.UI_NAMES.REPORT_PANEL, user);

        System.out.println("Exiting handleReportsAction Booking Panel");
    }

    @FXML
    private void handleLogoutAction(ActionEvent event) throws IOException {
        System.out.println("Entering handleLogoutAction Booking Panel");

        setUser(null);
        Util.showPanel(getClass(), Constants.UI_NAMES.LOGIN_PANEL);

        System.out.println("Exiting handleLogoutAction Booking Panel");
    }

    public void setUser(User user) {
        this.user = user;
        System.out.println("Booking User Set to --> " + user);

        if(user != null){
            user.setBookings(FileReader.getBookingsForUser(user));

            setBookingTableView();
        }
    }

    private void setAllDropdownValues() {
        System.out.println("Entering handleBookAction Booking Panel");

        resetExerciseDropdown();

        System.out.println("Exiting handleBookAction Booking Panel");

    }

    private void resetExerciseDropdown() {
        System.out.println("Entering handleBookAction Booking Panel");

        Map<String, Exercise> allExcercises = FileReader.getAllExercises();
        Collection<Exercise> exList = new ArrayList<Exercise>();

        for (Exercise ex : allExcercises.values()) {
            exList.add(ex);
        }

        exNameDrop.getItems().addAll(exList);

        System.out.println("Exiting handleBookAction Booking Panel");
    }

    private void resetExerciseDateDropdown() {
        System.out.println("Entering resetExerciseDateDropdown Booking Panel");
        dateDrop.getItems().clear();

        if (exercise != null) {
            Map<String, ExerciseDate> allDates = FileReader.getAllExDates();
            Collection<ExerciseDate> dateList = new ArrayList<ExerciseDate>();

            for (ExerciseDate date : allDates.values()) {
                dateList.add(date);
            }

            dateDrop.getItems().addAll(dateList);
        }

        System.out.println("Exiting resetExerciseDateDropdown Booking Panel");
    }

    private void resetExerciseSessionDropdown() {
        System.out.println("Entering resetExerciseSessionDropdown Booking Panel");
        sessionDrop.getItems().clear();

        if (date != null) {
            Map<String, ExerciseSession> allSessions = FileReader.getAllExSessions();
            Collection<ExerciseSession> sessionList = new ArrayList<ExerciseSession>();

            for (ExerciseSession session : allSessions.values()) {
                if (session.getDateId().equals(date.getDateId())) {
                    sessionList.add(session);
                }
            }

            sessionDrop.getItems().addAll(sessionList);
        }

        System.out.println("Exiting resetExerciseSessionDropdown Booking Panel");
    }

    private void setBookingTableView() {
        System.out.println("Entering setBookingTableView Booking Panel");

        if (allBookings.size() > 0) {
            allBookings.removeAll();
        }

        if (user.getBookings() != null && user.getBookings().size() > 0) {
            for (Booking booking : user.getBookings().values()) {
                allBookings.add(booking);
            }
        }

        myBookingTable.setItems(allBookings);
        exIdCol.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getExercise().getExId()));
        exNameCol.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getExercise().getExName()));
        dateCol.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getDate().getDate()));
        sessionCol.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getSession().getSession()));
        costCol.setCellValueFactory(d -> new SimpleDoubleProperty(d.getValue().getExercise().getCost()));

        System.out.println("Exiting setBookingTableView Booking Panel");
    }

    private void resetAll() {
        resetFields();
        resetMsg();
    }

    private void resetFields() {
        exercise = null;
        date = null;
        session = null;
        sessionDrop.getItems().clear();
        dateDrop.getItems().clear();
        exNameDrop.getItems().clear();

        resetExerciseDropdown();

        costField.setText("0.0");
    }

    private void resetMsg() {
        showErrorMsg("");
        showSuccessMsg("");
    }

    private void showErrorMsg(String msg) {
        failureMsgText.setText(msg);
    }

    private void showSuccessMsg(String msg) {
        successMsgText.setText(msg);
    }

    private boolean sessionTimeConflict(ExerciseSession session) {
        System.out.println("Entering sessionTimeConflict Booking Panel with session --> " + session);

        boolean isConflict = false;

        for (Booking booking : user.getBookings().values()) {
            if (booking.getSession().getSessionId().equals(session.getSessionId())) {
                isConflict = true;
                break;
            }
        }

        System.out.println("Exiting sessionTimeConflict Booking Panel with isConflict --> " + isConflict);
        return isConflict;
    }
}
