/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package excercisebooking;

import filehandler.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import model.Booking;
import model.Exercise;
import model.ExerciseDate;
import model.ExerciseSession;
import model.User;
import util.Constants;
import util.Util;

/**
 * FXML Controller class
 *
 * @author Iftekher Mahmud
 */
public class ReportsFXMLController implements Initializable {

    @FXML
    private ComboBox<Exercise> exNameDrop;
    @FXML
    private ComboBox<ExerciseDate> dateDrop;
    @FXML
    private ComboBox<ExerciseSession> sessionDrop;
    @FXML
    private TextField costField;
    @FXML
    private Button generateReportButton;
    @FXML
    private Text successMsgText;
    @FXML
    private Text failMsgText;
    @FXML
    private TableView<Booking> allBookingTable;
    @FXML
    private TableColumn<Booking, String> exIdCol;
    @FXML
    private TableColumn<Booking, String> exNameCol;
    @FXML
    private TableColumn<Booking, String> studentIdCol;
    @FXML
    private TableColumn<Booking, String> dateCol;
    @FXML
    private TableColumn<Booking, String> sessionCol;
    @FXML
    private TableColumn<Booking, Number> costCol;
    @FXML
    private Button backButton;
    @FXML
    private Button logoutButton;
    
    private User user;
    
    private Exercise exercise;
    private ExerciseDate date;
    private ExerciseSession session;
    
    private ObservableList<Booking> allBookings;
    @FXML
    private Button resetReportButton;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        System.out.println("Entering initialize Report Panel");
        
        costField.setText("0.0");
        costField.setEditable(false);

        setAllDropdownValues();

        allBookings = FXCollections.observableArrayList();
        System.out.println("Exiting initialize Report Panel");
    }    

    @FXML
    private void handleExNameAction(ActionEvent event) {
        System.out.println("Entering handleExNameAction Report Panel");
        
        exercise = exNameDrop.getSelectionModel().getSelectedItem();
        System.out.println("selExercise --> " + exercise);

        if (exercise != null) {
            costField.setText(exercise.getCost() + "");
            filterBookingsWith(Constants.FILTER_TYPES.FILTER_EXERCISE);
        }

        resetExerciseDateDropdown();
        
        System.out.println("Exiting handleExNameAction Report Panel");
    }

    @FXML
    private void handleDateAction(ActionEvent event) {
        System.out.println("Entering handleDateAction Report Panel");
        
        date = dateDrop.getSelectionModel().getSelectedItem();
        System.out.println("selDate --> " + date);
        
        filterBookingsWith(Constants.FILTER_TYPES.FILTER_EXERCISE_DATE);

        resetExerciseSessionDropdown();
        
        System.out.println("Exiting handleDateAction Report Panel");
    }

    @FXML
    private void handleSessionAction(ActionEvent event) {
        System.out.println("Entering handleSessionAction Report Panel");
        
        session = sessionDrop.getSelectionModel().getSelectedItem();
        
        filterBookingsWith(Constants.FILTER_TYPES.FILTER_EXERCISE_SESSION);
         
        System.out.println("Exiting handleSessionAction Report Panel");
    }

    @FXML
    private void handleGenerateButton(ActionEvent event) {
        System.out.println("Entering handleGenerateButton Report Panel");
        System.out.println("Exiting handleGenerateButton Report Panel");
    }

    @FXML
    private void handleBackButton(ActionEvent event) throws IOException {
        System.out.println("Entering handleBackButton Report Panel");
        
        Util.showBookingPanel(getClass(), Constants.UI_NAMES.BOOKING_PANEL, user);
        
        System.out.println("Exiting handleBackButton Report Panel");        
    }

    @FXML
    private void handleLogoutButton(ActionEvent event) throws IOException {
        System.out.println("Entering handleLogoutButton Report Panel");
        
        setUser(null);
        Util.showPanel(getClass(), Constants.UI_NAMES.LOGIN_PANEL);
        
        System.out.println("Exiting handleLogoutButton Report Panel");        
    }
    
    public void setUser(User user) {
        this.user = user;
        System.out.println("Report User Set to --> " + user);
        
        if(user != null){
//            user.setBookings(FileReader.getBookingsForUser(user));
            filterBookingsWith(Constants.FILTER_TYPES.FILTER_ALL);
            //setBookingTableView();
        }
    }
    
    private void setAllDropdownValues() {
        System.out.println("Entering handleBookAction Booking Panel");

        resetExerciseDropdown();

        System.out.println("Exiting handleBookAction Booking Panel");

    }

    private void resetExerciseDropdown() {
        System.out.println("Entering handleBookAction Booking Panel");

        Map<String, Exercise> allExcercises = FileReader.getAllExercises();
        Collection<Exercise> exList = new ArrayList<Exercise>();

        for (Exercise ex : allExcercises.values()) {
            exList.add(ex);
        }

        exNameDrop.getItems().addAll(exList);

        System.out.println("Exiting handleBookAction Booking Panel");
    }

    private void resetExerciseDateDropdown() {
        System.out.println("Entering resetExerciseDateDropdown Booking Panel");
        dateDrop.getItems().clear();

        if (exercise != null) {
            Map<String, ExerciseDate> allDates = FileReader.getAllExDates();
            Collection<ExerciseDate> dateList = new ArrayList<ExerciseDate>();

            for (ExerciseDate date : allDates.values()) {
                dateList.add(date);
            }

            dateDrop.getItems().addAll(dateList);
        }

        System.out.println("Exiting resetExerciseDateDropdown Booking Panel");
    }

    private void resetExerciseSessionDropdown() {
        System.out.println("Entering resetExerciseSessionDropdown Booking Panel");
        sessionDrop.getItems().clear();

        if (date != null) {
            Map<String, ExerciseSession> allSessions = FileReader.getAllExSessions();
            Collection<ExerciseSession> sessionList = new ArrayList<ExerciseSession>();

            for (ExerciseSession session : allSessions.values()) {
                if (session.getDateId().equals(date.getDateId())) {
                    sessionList.add(session);
                }
            }

            sessionDrop.getItems().addAll(sessionList);
        }

        System.out.println("Exiting resetExerciseSessionDropdown Booking Panel");
    }
    
    private void filterBookingsWith(int filterType) {
        System.out.println("Entering filterBookingsWith filterType --> " + filterType);
        allBookings.removeAll();
//        if (allBookings.size() > 0) {
//            allBookings.removeAll();
//        }
        allBookings.clear();
        
        System.out.println("allBookings size --> " + allBookings.size());
        
        Map<String, Booking> bookingList = FileReader.getAllBookings();
        
        switch(filterType) {
            case Constants.FILTER_TYPES.FILTER_ALL:
                System.out.println("Adding for FILTER_ALL");
                for (Booking booking : bookingList.values()) {
                    allBookings.add(booking);
                }
                break;
                
            case Constants.FILTER_TYPES.FILTER_EXERCISE:
                System.out.println("Adding for FILTER_EXERCISE");
                for (Booking booking : bookingList.values()) {
                    if(booking.getExercise().getExId().equals(exercise.getExId())){
                        allBookings.add(booking);
                    }
                }
                break;
                
            case Constants.FILTER_TYPES.FILTER_EXERCISE_DATE:
                System.out.println("Adding for FILTER_EXERCISE_DATE");
                for (Booking booking : bookingList.values()) {
                    if(booking.getExercise().getExId().equals(exercise.getExId()) && booking.getDate().getDateId().equals(date.getDateId())){
                        allBookings.add(booking);
                    }
                }
                break;
                
            case Constants.FILTER_TYPES.FILTER_EXERCISE_SESSION:
                System.out.println("Adding for FILTER_EXERCISE_SESSION");
                for (Booking booking : bookingList.values()) {
                    if(booking.getExercise().getExId().equals(exercise.getExId()) && booking.getSession().getSessionId().equals(session.getSessionId())){
                        allBookings.add(booking);
                    }
                }
                break;
        }
        System.out.println("allBookings size --> " + allBookings.size());
        System.out.println("Exiting filterBookingsWith allBookings --> " + allBookings);
        setBookingTableView();
    }
    
    private void setBookingTableView() {
        System.out.println("Entering setBookingTableView Booking Panel");

//        if (user.getBookings() != null && user.getBookings().size() > 0) {
//            for (Booking booking : user.getBookings().values()) {
//                allBookings.add(booking);
//            }
//        }

        allBookingTable.setItems(allBookings);
        exIdCol.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getExercise().getExId()));
        exNameCol.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getExercise().getExName()));
        studentIdCol.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getUser().getUsername()));
        dateCol.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getDate().getDate()));
        sessionCol.setCellValueFactory(d -> new SimpleStringProperty(d.getValue().getSession().getSession()));
        costCol.setCellValueFactory(d -> new SimpleDoubleProperty(d.getValue().getExercise().getCost()));

        System.out.println("Exiting setBookingTableView Booking Panel");
    }

    private void resetAll() {
        resetFields();
        resetMsg();
    }

    private void resetFields() {
        exercise = null;
        date = null;
        session = null;
        sessionDrop.getItems().clear();
        dateDrop.getItems().clear();
        exNameDrop.getItems().clear();

        resetExerciseDropdown();

        costField.setText("0.0");
    }

    private void resetMsg() {
        showErrorMsg("");
        showSuccessMsg("");
    }

    private void showErrorMsg(String msg) {
        failMsgText.setText(msg);
    }

    private void showSuccessMsg(String msg) {
        successMsgText.setText(msg);
    }

    @FXML
    private void handleResetButton(ActionEvent event) {
        resetFields();
        filterBookingsWith(Constants.FILTER_TYPES.FILTER_ALL);
    }
}
