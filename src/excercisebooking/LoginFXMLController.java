/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXML2.java to edit this template
 */
package excercisebooking;

import filehandler.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import model.User;
import util.Constants;
import util.Util;

/**
 *
 * @author Iftekher Mahmud
 */
public class LoginFXMLController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Button loginButton;
    
    private Map<String, User> allUsers;
    @FXML
    private Text failureMsgText;
        
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("Entering initialize Login Panel");
        
        allUsers = FileReader.getAllUsersWithBookings();
        
        System.out.println("Exiting initialize Login Panel");
    }    

    @FXML
    private void handleLoginAction(ActionEvent event) throws IOException {
        System.out.println("Entering handleLoginAction");
        
        resetErrMsg();
        
        User user = new User();
        user.setUsername(usernameField.getText());
        user.setPassword(Util.getHashString(passwordField.getText()));
        
        if(isUserMatched(user)) {
            Util.showBookingPanel(getClass(), Constants.UI_NAMES.BOOKING_PANEL, user);
        } else {
            showLoginErrMsg();
        }
        
        System.out.println("Exiting handleLoginAction");
    }
    
    private boolean isUserMatched(User user) {
        System.out.println("Entering isUserMatched with user --> " + user);
        
        boolean isMatched = false;
        
        for (User storedUser: allUsers.values()) {
            if(storedUser.getUsername().equals(user.getUsername()) && storedUser.getPassword().equals(user.getPassword())){
                isMatched = true;
                break;
            }
        }
        
        System.out.println("Exiting isUserMatched with isMatched --> " + isMatched);
        return isMatched;
    }
    
    private void showLoginErrMsg() {
        failureMsgText.setText(Constants.ERR_MSG.LOGIN_FAILED);
    }
    
    private void resetFields() {
        resetErrMsg();
        resetUsernameField();
        resetPasswordField();
    }
    
    private void resetErrMsg() {
        failureMsgText.setText("");
    }
    
    private void resetUsernameField() {
        usernameField.setText("");
    }
    
    private void resetPasswordField() {
        passwordField.setText("");
    }
}
