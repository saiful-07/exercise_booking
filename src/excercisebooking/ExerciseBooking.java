/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXML.java to edit this template
 */
package excercisebooking;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.Constants;

/**
 *
 * @author Iftekher Mahmud
 */
public class ExerciseBooking extends Application {
    private static Stage mainStage;
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource(Constants.UI_NAMES.LOGIN_PANEL));
        
        Scene scene = new Scene(root);
        mainStage = stage;
//        scene.getStylesheets().add(getClass().getResource("../css/ex_booking.css").toExternalForm());
        scene.getStylesheets().add(getClass().getResource(Constants.UI_NAMES.CSS).toExternalForm());
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public static Stage getMainStage(){
        return mainStage;
    }
}
